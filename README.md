Hand-picked presentations and summary of each from SolidWorks World 2019.


This is the latest exhibition contains the word "SolidWorks". From now on it will be called as "3D EXPERIENCE World". If you want to learn how community feels about this, check the forum link below.


https://forum.solidworks.com/message/937720

<hr>

- [2019 - 12791 - Real Good Tips Real Bad Jokes](#2019-12791-real-good-tips-real-bad-jokes)
- [2019 - 12972 - Automating SOLIDWORKS Assemblies Using Reference Geometry and API](#2019-12972-automating-solidworks-assemblies-using-reference-geometry-and-api)
- [2019 - 13004 - This Is Not a Toy SOLIDWORKS Assembly Tools Taught with Legos](#2019-13004-this-is-not-a-toy-solidworks-assembly-tools-taught-with-legos)
- [2019 - 13032 - SOLIDWORKS xDesign Basics](#2019-13032-solidworks-xdesign-basics)
- [2019 - 13171 - The Large Assembly Beast Tips to Tame your Large Assemblies](#2019-13171-the-large-assembly-beast-tips-to-tame-your-large-assemblies)
- [2019 - 13499 - Macros vs Add-ins vs Stand-Alones](#2019-13499-macros-vs-add-ins-vs-stand-alones)
- [2019 - 13632 - Stop Waiting, Start Working How to Make Large Assemblies Faster](#2019-13632-stop-waiting-start-working-how-to-make-large-assemblies-faster)
- [2019 - 13650 - Sweeping Away the Competition](#2019-13650-sweeping-away-the-competition)
- [2019 - 13804 - Achieving Extreme SOLIDWORKS Performance](#2019-13804-achieving-extreme-solidworks-performance)

<hr>

Sign up to SolidWorks World website which doesn't require ongoing subscription service to get any data and check cited content with given presentation code.


https://solidworksworld.lanyonevents.com/2019/connect/search.ww


![1](img/intro-1.png)

![2](img/intro-2.png)

![3](img/intro-3.png)

<hr>

You can also check simplified versions of 2017 and 2018 presentations I prepared.


https://gitlab.com/fatihmehmetozcan/solidworks-tips-tricks

<hr>

Go to https://fatihmeh.github.io/solidworks-world-2019/ to see this content as a classic web page based on Github Pages

<hr>

### 2019 - 12791 - Real Good Tips Real Bad Jokes

![1](img/2019-SWW-12791_01.png)

![2](img/2019-SWW-12791_02.png)

![3](img/2019-SWW-12791_03.png)

<hr>

### 2019 - 12972 - Automating SOLIDWORKS Assemblies Using Reference Geometry and API

![1](img/2019-SWW-12972_01.png)

![2](img/2019-SWW-12972_02.png)

![3](img/2019-SWW-12972_03.png)

<hr>

### 2019 - 13004 - This Is Not a Toy SOLIDWORKS Assembly Tools Taught with Legos

![1](img/2019-SWW-13004_01.png)

![2](img/2019-SWW-13004_02.png)

![3](img/2019-SWW-13004_03.png)

![4](img/2019-SWW-13004_04.png)

![5](img/2019-SWW-13004_05.png)

<hr>

### 2019 - 13032 - SOLIDWORKS xDesign Basics

![1](img/2019-SWW-13032_01.png)

![2](img/2019-SWW-13032_02.png)

![3](img/2019-SWW-13032_03.png)

![4](img/2019-SWW-13032_04.png)

<hr>

### 2019 - 13171 - The Large Assembly Beast Tips to Tame your Large Assemblies

![1](img/2019-SWW-13171_01.png)

![2](img/2019-SWW-13171_02.png)

![3](img/2019-SWW-13171_03.png)

![4](img/2019-SWW-13171_04.png)

<hr>

### 2019 - 13499 - Macros vs Add-ins vs Stand-Alones

![1](img/2019-SWW-13499_01.png)

<hr>

### 2019 - 13632 - Stop Waiting Start Working How to Make Large Assemblies Faster

![1](img/2019-SWW-13632_01.png)

![2](img/2019-SWW-13632_02.png)

![3](img/2019-SWW-13632_03.png)

![4](img/2019-SWW-13632_04.png)

![5](img/2019-SWW-13632_05.png)

<hr>

### 2019 - 13650 - Sweeping Away the Competition

![1](img/2019-SWW-13650_01.png)

<hr>

### 2019 - 13804 - Achieving Extreme SOLIDWORKS Performance

![1](img/2019-SWW-13804_01.png)

![2](img/2019-SWW-13804_02.png)

![3](img/2019-SWW-13804_03.png)

![4](img/2019-SWW-13804_04.png)

![5](img/2019-SWW-13804_05.png)

![6](img/2019-SWW-13804_06.png)

![7](img/2019-SWW-13804_07.png)

![8](img/2019-SWW-13804_08.png)